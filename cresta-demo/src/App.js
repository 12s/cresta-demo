import React, { Component } from 'react';
import ClientCards from './ClientCards';

// import { subscribeToTimer } from './api'; // not working :(
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clients: [{
                name: "Client 1",
                messages: [
                    {
                        time: new Date().toLocaleTimeString(),
                        message: "Hello, I'm interested in buying your product."
                    }
                ]
            }]
        }
        this.addClient = this.addClient.bind(this);
    }
    
    render() {
        return (
          <div className="App">

              <h2 className="ui header">
                  <i className="settings icon"></i>
                  <div className="content">
                      Cresta Sandbox Chat App
                      <div className="sub header">Proof of Concept Interview Exam</div>
                  </div>

                  <div className="actions">
                      <button className="ui basic button" onClick={this.addClient}>
                          <i className="user icon"></i>
                          Add Client
                      </button>

                      <button className="ui basic button">
                          <i className="bullhorn icon"></i>
                          Generate Chat
                      </button>
                  </div>
              </h2>

              <div className="outlet">
                  <ClientCards clients={this.state.clients}/>
              </div>

              <div className="footer">
                <Tick />
              </div>
          </div>
        );
    }
    
    /**
     * Add a Client Card to application.
     * @method addClient
     * @returns {void}
     * @private
     */
    addClient() {
        const clientCount = this.state.clients.length;

        this.setState((prevState) => {
            if (clientCount < 3) {
                return prevState.clients.push({name: `Client ${this.state.clients.length + 1}`});
            }
        });
    }
}

function Tick() {
    return(
        <div>
            <h2>It is {new Date().toLocaleTimeString()}.</h2>
        </div>
    );

}

setInterval(Tick, 1000);

export default App;
