import React from 'react';
import './ClientCards.css';

class ClientCards extends React.Component {
    render() {
        return (
            <Clients clients={this.props.clients}/>
        )
    }
}

function Clients(props) {
    let items = [];

    for (let i = 0; i < props.clients.length; i++) {
        items.push(
            props.clients[i].name
        );
    }
    return items.map((client, index) => <Client key={index} info={client}/>);
}

function Client(props) {
    /**
     * Add a Client Card to application.
     * @method addClient
     * @returns {void}
     * @private
     */



    return <h4 className="clientWrapper">
                <i className="user circle outline icon"></i>
                {props.info}
                <button className="removeClient" attribute={props.info} onClick={removeClient}>
                    <i className="trash icon"></i>
                </button>
            </h4>
}

function removeClient(e) {

    // Update state to remove client
}

export default ClientCards;
